# Pull ROS2 Foxy DOcker Image

```
docker pull ros:foxy-ros1-bridge
```

# Create container from docker Image

```
docker run -it --name ros2foxy --net host ros:foxy-ros1-bridge bash
```

# 
